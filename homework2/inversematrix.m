function invA = inversematrix(A)
    % obtain row echelon and rre of the input matrix
    [rA,~] = rowechelon(A);
    [rrA,~,~] = reducedrowechelon(rA); 
    [row,col] = size(rrA);

    % determine if input matrix is invertible
    if row~=col
        error('Matrix is singular, not invertible.')
        return
    elseif ~isequal(rrA,eye(col))
        error('Matrix is singular, not invertible.')
        return
    end

    % perform gaussian elimination to obtain inverse of the input matrix
    IA = [A,eye(row)];
    [rIA,~] = rowechelon(IA);
    [rrIA,~,~] = reducedrowechelon(rIA); 
    invA = rrIA(:,col+1:end);
end