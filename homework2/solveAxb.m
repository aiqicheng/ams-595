function x = solveAxb(A,b)
    % check if input has matching dimensions
    [m,n] = size(A);
    lm = length(b);
    if m~=lm
        error('The two inputs do not have the same number of rows')
        return
    end
    
    sys = [A,b];
    [rsys,~] = rowechelon(sys);
    % obtain the rref of the equation system and its pivot locations
    [rsys,pivot,~] = reducedrowechelon(rsys); 
    rA = rsys(:,1:n);
    rb = rsys(:,n+1); 
    % check if reduced system has invalid equations (i.e. 0 = nonzero)
    for p = m:-1:1
        if pivot(p) == n+1
            error('Inconsistent system of equations. No solution')
            return
        end
    end
    
    % check if solution unique (if there are free variables in the system)
    if rank(rA) ~= n
        warning('Dependent system, Solution not unique') 
    end
    x = rA\rb;
 end 
