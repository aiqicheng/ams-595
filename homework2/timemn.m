
% suppose we have m and n ranging from 2:10
time = zeros(20,20);
totaltime = zeros(20,20);
mlist = [1:20];
nlist = [1:20];
% the time result need to be averaged
iterations = 50;
for it = 1: iterations
    for m = 2:20
        for n = 2:20
            tic
            A = rand(m,n);
            b = rand(m,1);
            x = solveAxbwerr(A,b);
            toc
            time(m,n) = toc;
        end
    end
    totaltime = totaltime+time;
end
time = totaltime/iterations;
figure
surf(mlist,nlist,time,'FaceAlpha',0.5)
xlabel('m')
ylabel('n')
zlabel('time')
title('time required to solve the system as function of random sized matrices')





% solve Ax=b without exiting on error.
function x = solveAxbwerr(A,b)
    % check if input has matching dimensions
    [m,n] = size(A);
    lm = length(b);
    if m~=lm
        error('The two inputs do not have the same number of rows')
        return
    end
    
    sys = [A,b];
    [rsys,~] = rowechelon(sys);
    % obtain the rref of the equation system and its pivot locations
    [rsys,pivot,~] = reducedrowechelon(rsys); 
    rA = rsys(:,1:n);
    rb = rsys(:,n+1); 
    % check if reduced system has invalid equations (i.e. 0 = nonzero)
    for p = m:-1:1
        if pivot(p) == n+1
            warning('Inconsistent system of equations. No solution')
            % return 
            % did not error or return, becuase continue plot of the function
        end
    end
    
    % check if solution unique (if there are free variables in the system)
    if rank(rA) ~= n
        warning('Dependent system, Solution not unique') 
    end
    x = rA\rb;
 end 
