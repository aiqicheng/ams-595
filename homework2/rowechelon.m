function  [out, elementary] = rowechelon(A)
    [row,col] = size(A);
    % if A is already in row echelon form, output A directly
    if isequal(howechelon(A),'row echelon')||isequal(howechelon(A),'row canonical')
        out = A;
        elementary = eye(row);
        return
    end
    
    ele = eye(row);
    j = 1;
    skip = 0;
    while j <= col
        i = j-skip;
        while i <= row % in this i loop, i doesn't change
            if (A(i,j)~=0)&&(i<row) % then a_ij is a pivot, it is used to zero out entries below
                for k = i+1:row
                    ele(k,:) = ele(k,:) - (A(k,j)/A(i,j))*ele(i,:);
                    A(k,:) = A(k,:) - (A(k,j)/A(i,j))*A(i,:);
                    % enforcement to avoid rounding error propagate
                    A(k,j) = 0; 
                end
                break
            elseif i==row
                break
            else
                %k = i;
                for k = i+1:row
                    if A(k,j)~=0                
                        thisrow = A(i,:);
                        thatrow = ele(i,:);
                        A(i,:) = A(k,:);
                        ele(i,:) = ele(k,:);
                        A(k,:) = thisrow;
                        ele(k,:) = thatrow;
                    end
                end
                % if this column is all zero, then A 
                if isequal(A(i:end,j),zeros(row-i+1,1))
                    skip = skip+1;
                    break
                end
            end
        end
        j = j+1;
    end
   out = A;
   elementary = ele;
end