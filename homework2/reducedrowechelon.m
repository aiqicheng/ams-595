function [out,pivot,elementary] = reducedrowechelon(A)
    [row,col] = size(A);
    ele = eye(row);
    if ~isequal(howechelon(A),'row echelon') && ~isequal(howechelon(A),'row canonical')
        error('Input not an echelon form')
        return
    end

    % locate  pivot in each row
    pivot = zeros(row,1);
    for i = 1:row
        for j = 1:col
            if A(i,j)~=0
                pivot(i) = j;
                break
            end
        end
    end
    
    % use backward substitution to clear out entries in the pivot columns
    for i = row:-1:1
        if pivot(i)==0
            continue
        else
            ele(i,:) = ele(i,:)/A(i,pivot(i));
            A(i,:) = A(i,:)/A(i,pivot(i)); % normalize pivot of this row
            if i>1
                for k = i-1:-1:1 % clear out entries above
                    ele(k,:) = ele(k,:)-A(k,pivot(i))*ele(i,:);
                    A(k,:) = A(k,:)-A(k,pivot(i))*A(i,:);
                end
            end
        end
    end
    out = A;
    elementary = ele;
end