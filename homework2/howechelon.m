% input matrix A 
% is A row echelon? 
%   - No --> A is neither
%   - Yes --> is A row canonical?
%               - No --> A is row echelon
%               - Yes --> A is row canonical

function how = howechelon(A)
    [row, col] = size(A);
    how = 0;
    % pivot of each row determines the type of matrix
    pivot = ones(row, 1); % indices of pivot from each row
    % 1st row, if it's a zero matrix --> row canonical
    %          elseif find pivot~=0
    
    for j = 1:col
        if isequal(A, zeros(row,col))
            how = 'row canonical';
            return
         elseif isequal(A(1,:),zeros(1,col))
            how = 'neither';
            return
        elseif A(1,j) ~= 0 
            pivot(1) = j;
            break
        end
    end 

    for i = 2:row
        for j = 1:col
            if A(i,j) ~= 0
                pivot(i) = j;
                break
            elseif (A(i,j)==0)&&(j==col)
                pivot(i) = j+1; % representing a row of all zeros
            end
        end
        if pivot(i-1) < pivot(i)
            continue
        elseif (pivot(i)==pivot(i-1))&&(pivot(i)==col+1)
            continue
        else 
            how = 'neither';
            return
        end
    end

    % after finding pivot of each row, 
    % if how=!'neither', A is at least row echelon
    % if all the pivot has value 1 and is the only nonzero in itscolumn,
    % A is row caconical
    for i = 1:row
        j = pivot(i);
        if j<=col
            if A(i,j) ~= 1
                how = 'row echelon';
                return
            elseif sum(A(:,j))~=A(:,j)
                how = 'row echelon';
                return
            end
        end
    end
    how = 'row canonical';    
end
